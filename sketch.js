let seeds = [];
let flowers = ['M', 'A', 'I', 'S', 'Y']; // Letters of Maisy's name
let growthStage = Array(5).fill(0); // Adjusted for 5 letters
let clouds = [];
let birthdayMessageShown = false;
let flowerColors = ['#FF6384', '#36A2EB', '#FFCE56', '#4BC0C0', '#9966FF']; // Array of colors for each flower

function setup() {
  createCanvas(800, 400);
  // Initialize seed positions and clouds
  for (let i = 0; i < flowers.length; i++) {
    seeds.push(createVector(100 + i * 120, height - 50)); // Adjust spacing to fit 5 seeds
  }
  // Initialize clouds
  for (let i = 0; i < 5; i++) {
    let x = random(width);
    let y = random(50, 150); // Clouds at varying heights
    let size = random(50, 100);
    let speed = random(0.5, 1.5);
    clouds.push({x, y, size, speed});
  }
}

function draw() {
  background(200, 225, 255); // Light blue sky background
  drawClouds();
  drawDirt();
  drawSeeds();
  drawFlowers();
  if (allFlowersShown()) {
    drawBirthdayMessage();
  }
}

function drawClouds() {
  fill(255); // White clouds
  noStroke();
  clouds.forEach(cloud => {
    ellipse(cloud.x, cloud.y, cloud.size, cloud.size / 2);
    ellipse(cloud.x + cloud.size * 0.5, cloud.y - cloud.size * 0.3, cloud.size * 0.5, cloud.size * 0.25);
    ellipse(cloud.x - cloud.size * 0.5, cloud.y - cloud.size * 0.1, cloud.size * 0.6, cloud.size * 0.3);
    cloud.x += cloud.speed; // Move the cloud along
    // Wrap clouds around the screen
    if (cloud.x > width + 50) {
      cloud.x = -50;
    }
  });
}

function drawDirt() {
  noStroke();
  fill(139, 69, 19); // Dirt color
  rect(0, height - 70, width, 70); // Draw a clean layer of dirt
}

function drawSeeds() {
  fill(80, 42, 12); // Significantly darker brown for seeds
  for (let i = 0; i < seeds.length; i++) {
    ellipse(seeds[i].x, seeds[i].y, 15, 15);
  }
}

function drawFlowers() {
  for (let i = 0; i < flowers.length; i++) {
    if (growthStage[i] > 110) { // If the seed has grown enough
      fill(0, 255, 0); // Green color for stalk
      rect(seeds[i].x - 2, seeds[i].y - growthStage[i] +20, 4, growthStage[i] - 20); // Draw stalk
      fill(flowerColors[i]); // Different colors for each flower
      drawLetter(flowers[i], seeds[i].x, seeds[i].y - growthStage[i]);
    }
    else if (growthStage[i] > 0) {
      fill(0, 255, 0); // Green color for stalk
      rect(seeds[i].x - 2, seeds[i].y - growthStage[i], 4, growthStage[i]); // Draw stalk
    }
  }
}

function drawLetter(letter, x, y) {
  noStroke();
  switch (letter) {
  case 'M':
    drawM(x, y);
    break;
  case 'A':
    drawA(x, y);
    break;
  case 'I':
    drawI(x, y);
    break;
  case 'S':
    drawS(x, y);
    break;
  case 'Y':
    drawY(x, y);
    break;
  }
}

function allFlowersShown() {
  return growthStage.every(stage => stage > 110);
}

function drawBirthdayMessage() {
  fill(0);
  textSize(24);
  text("Happy 7th Birthday Maisy!!", width / 2 - 170, height - 20);
}

// Functions to draw letters as detailed flowers using small ellipses
function drawM(x, y) {

  ellipse(x - 10, y + 5, 10, 10);
  ellipse(x + 10, y + 5, 10, 10);
  ellipse(x - 15, y + 20, 10, 10);
  ellipse(x + 15, y + 20, 10, 10);
  ellipse(x, y + 15, 10, 10);
}

function drawA(x, y) {
  ellipse(x, y, 10, 10);
  ellipse(x - 10, y + 10, 10, 10);
  ellipse(x + 10, y + 10, 10, 10);
  ellipse(x, y + 15, 10, 10);
  ellipse(x - 10, y + 20, 10, 10);
  ellipse(x + 10, y + 20, 10, 10);
}

function drawI(x, y) {
  ellipse(x, y, 10, 10);
  ellipse(x, y + 10, 10, 10);
  ellipse(x, y + 20, 10, 10);
}

function drawS(x, y) {
  ellipse(x, y - 2, 10, 10);
  ellipse(x + 7, y, 10, 10);

  ellipse(x - 7, y + 3, 10, 10);
  ellipse(x, y + 10, 10, 10);
  ellipse(x + 7, y + 17, 10, 10);

  ellipse(x, y + 22, 10, 10);
  ellipse(x - 7, y + 20, 10, 10);
}

function drawY(x, y) {
  ellipse(x, y + 22, 10, 10);
  ellipse(x - 10, y, 10, 10);
  ellipse(x + 10, y, 10, 10);
  ellipse(x, y + 10, 10, 10);
}

function mousePressed() {
  handlePress(mouseX, mouseY);
}

function touchStarted() {
  handlePress(touches[0].x, touches[0].y);
  return false; // This prevents default behavior, like scrolling
}

function handlePress(x, y) {
  for (let i = 0; i < seeds.length; i++) {
    if (dist(x, y, seeds[i].x, seeds[i].y) < 20) {
      growthStage[i] += 40; // Increase growth stage if a seed is clicked or touched
    }
  }
}
